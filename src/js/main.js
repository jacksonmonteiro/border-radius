const alteration = document.getElementById('alteration');
const code = document.getElementById('myInput')

function apply(){
	const top = document.getElementById('topLeft').value;
	const right = document.getElementById('topRight').value;
	const bottom = document.getElementById('downRight').value;
	const left = document.getElementById('downLeft').value;

	// altera o 'border-radius' de alteration
	alteration.style.borderRadius = `${top}px ${right}px ${bottom}px ${left}px`;

	showCode(top, right, bottom, left);
}

function showCode(t, r, b, l){
	const codeNoPrefix = `${t}px ${r}px ${b}px ${l}px`;
	code.value = codeNoPrefix;
}

function copy(){
	// recebe o campo de texto
	let copied = document.getElementById('myInput');

	//Seleciona o campo de texto
	copied.select();

	//copia o texto dentro do campo de texto
	document.execCommand('copy');

	//Avisa ao usuário que o texto foi copiado
	window.alert("Copied: " + copied.value);
}